package com.amdocs;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {



    @Test
    public void testAdd() throws Exception {

        final int testing = new Calculator().add();
        assertEquals("Add", 9, testing);
        
    }
    @Test   
    public void testSub() throws Exception {

        final int testing = new Calculator().sub();
        assertEquals("Sub", 3, testing);

    }
}
