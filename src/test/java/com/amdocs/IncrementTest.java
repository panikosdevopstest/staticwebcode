package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;


public class IncrementTest {
    @Test
    public void testGetCounter() throws Exception {

        final int increment= new Increment().getCounter();
        assertEquals("Get", increment, 1);
        
    }
    @Test   
    public void testDecreasecounterZero() throws Exception {

        final Increment increment =  new Increment();

        increment.decreasecounter(0);
        final int testing = increment.decreasecounter(0);

        assertEquals("Sub", 0, testing);

    }

    @Test
    public void testDecreasecounterOne() throws Exception {

        final Increment increment =  new Increment();

        increment.decreasecounter(1);
        final int testing = increment.decreasecounter(1);

        assertEquals("Add", 1, testing);

    }
    
    @Test
    public void testDecreasecounter() throws Exception {

        final Increment increment =  new Increment();

        increment.decreasecounter(2);
        int testing = increment.decreasecounter(-1);

        assertEquals("Bad input", 2, testing);

    }
}
