package com.amdocs;

public class Increment {
	
	private int counter = 1;
	  
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		
		switch (input) {
			case 0:
				return counter--; 
			case 1:
				return counter;
			default:
				return counter++;
		}
		
	}			
}
